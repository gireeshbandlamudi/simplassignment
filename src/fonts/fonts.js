export const Fonts = {
    sansProSemiBold: 'SourceSansPro-SemiBold',
    sansProRegular: 'SourceSansPro-Regular',
    sansProBold: 'SourceSansPro-Bold'
}