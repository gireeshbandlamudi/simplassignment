import React, {PureComponent} from 'react';
import {
    View,   
} from 'react-native';

import { styles } from './appStyles';

export default class RadioButton extends PureComponent {
    render(){
        return(
            <View>
            {
                this.props.selected ? (
                    <View style={styles.selectBorder}>
                        <View style={styles.selectBorderHighlight} />
                    </View>
                ) : (
                    <View style={styles.planeBorder} />
                )
            }
            </View>
        )
    }
}