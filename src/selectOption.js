import React, {PureComponent} from 'react';
import {
    TouchableOpacity,
    View,
    Text,    
} from 'react-native';

import { styles } from './appStyles';
import RadioButton from './radioButton';

import { Fonts } from './fonts/fonts';

export default class SelectOption extends PureComponent {
    render(){
        return(
            <TouchableOpacity 
            style={styles.optionStyle}
            onPress={()=> this.props.onSelectOption(this.props.optionId)}
            >
                <View style={{marginLeft: 20,}}>
                    <RadioButton selected={this.props.optionSelected} />
                </View>
                <View style={{marginLeft: 10,}}>
                    <Text style={{fontSize: 15, fontFamily: Fonts.sansProSemiBold}}>{this.props.optionHeading}</Text>
                    <Text style={{fontSize: 13, fontFamily: Fonts.sansProRegular, color: 'rgb(118, 137, 138)', }}>{this.props.optionDescription}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}