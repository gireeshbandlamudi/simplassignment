import { StyleSheet, Dimensions } from 'react-native';
const SCREEN_HEIGHT = Dimensions.get('window').height;

import { Fonts } from './fonts/fonts';

export const styles = StyleSheet.create({
    selectBorder: {
        width: 20, 
        height: 20, 
        borderRadius: 15, 
        borderWidth: 1, 
        borderColor: 'rgb(0, 209, 194)', 
        justifyContent: 'center', 
        alignItems: 'center', 
    },
    selectBorderHighlight: {
        width: 12, 
        height: 12, 
        borderRadius: 6, 
        backgroundColor: 'rgb(0, 209, 194)', 
    },
    planeBorder: {
        width: 20, 
        height: 20, 
        borderRadius: 15, 
        borderWidth: 1, 
        borderColor: '#000000', 
        justifyContent: 'center', 
        alignItems: 'center', 
    },
    optionStyle: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: 10, 
        marginBottom: 10, 
    },
    safeAreaView: {
        flex: 1, 
        backgroundColor: 'rgb(0, 187, 180)'
    },
    backgroundUpperPart: {
        width: '100%', 
        height: ((SCREEN_HEIGHT * 25) / 100), 
        // backgroundColor: 'rgb(0, 210, 213)' 
    },
    backgroundLowerPart: {
        flex: 1, 
        backgroundColor: 'rgb(244, 244, 244)'
    },
    absoluteStyle: {
        position: 'absolute', 
        width: '100%',  
    },
    buttonStyle: {
        width: '90%', 
        height: 52, 
        borderRadius: 4,  
    },
    buttonTextStyle: {
        color: '#ffffff', 
        fontSize: 16, 
        fontWeight: 'bold', 
        fontFamily: Fonts.sansProSemiBold,
    },
    optionsField: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15, 
    },
    optionsHeading: {
        width: '90%', 
        height: 30, 
        borderTopLeftRadius: 5, 
        borderTopRightRadius: 5, 
        borderBottomWidth: 1, 
        borderBottomColor: '#dddddd', 
        backgroundColor: '#ffffff', 
        justifyContent: 'center', 
    },
    optionsHeadingTextStyle: {
        marginLeft: 20, 
        fontSize: 13, 
        color: 'rgb(118, 137, 138)',
        fontFamily: Fonts.sansProRegular,
    },
    optionViewStyle: {
        width: '90%', 
        backgroundColor: '#ffffff', 
        borderBottomLeftRadius: 5, 
        borderBottomRightRadius: 5, 
    },
    textInputField: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15, 
    },
    textInputFieldBox: {
        width: '90%', 
        height: 64, 
        borderRadius: 4, 
        backgroundColor: 'rgb(0, 193, 197)', 
        flexDirection: 'row', 
        alignItems: 'center', 
        borderWidth: 1, 
        borderColor: 'rgb(50, 182, 178)'
    },
    textStyle: {
        fontSize: 30, 
        color: 'rgb(255, 255, 255)', 
        fontWeight: 'bold', 
        marginLeft: 10, 
        fontFamily: Fonts.sansProSemiBold,
    },
    textInputStyle: {
        fontSize: 24, 
        padding: 10, 
        color: 'rgb(255, 255, 255)', 
        fontWeight: 'bold', 
        width: '100%', 
        fontFamily: Fonts.sansProBold,
    },
    detailsField: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        marginTop: 20, 
    },
    detailsFieldFlex: {
        flexDirection: 'row', 
        marginLeft: 20, 
        alignItems: 'center', 
    },
    buttonStyle1: {
        width: 81, 
        height: 32, 
        backgroundColor: 'rgb(255, 255, 255)', 
        borderRadius: 4, 
        justifyContent: 'center', 
        alignItems: 'center', 
        marginLeft: 10, 
    },
    buttonStyle1Text: {
        color: '#00d1c1', 
        fontSize: 12, 
        fontFamily: Fonts.sansProSemiBold
    },
    usernameStyle: {
        color: '#ffffff', 
        fontSize: 18, 
        fontWeight: 'bold', 
        fontFamily: Fonts.sansProSemiBold,
    },
    mobileNumberStyle: {
        color: '#ffffff', 
        fontSize: 13,
        fontFamily: Fonts.sansProRegular,
    },
    closeButtonStyle: {
        color: 'rgb(233, 233, 233)', 
        fontSize: 20, 
        fontWeight: 'bold', 
    },
});