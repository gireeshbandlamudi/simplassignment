import React, {Component} from 'react';
import { 
  Text, 
  View,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Keyboard,
  Animated,
  StyleSheet,
  Platform,
  Image,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import SelectOption from './src/selectOption';
import { styles } from './src/appStyles';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      amount: "",
      paymentOptions: [
        {
          optionId: 0,
          optionHeading: "Simpl:₹2000 available",
          optionDescription: "₹1922 will be charged to your account",
          optionSelected: true,
        },
        {
          optionId: 1,
          optionHeading: "UPI",
          optionDescription: "A payment request will be sent to your UPI",
          optionSelected: false,
        }
      ],
    }
  }

  componentWillMount(){

    // Keyboard methods for IOS
    this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);

    // Keyboard methods for Android
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);

    this.keyboardHeight= new Animated.Value(5);

  }

  // Code for keyboardWillShow method
  keyboardWillShow = (event) => {
    if(Platform.OS === 'android'){
      duration = 100
    } else{
      duration = event.duration
    }
    Animated.timing(this.keyboardHeight, {
      duration: duration + 100,
      toValue: event.endCoordinates.height + 10,
    }).start()
  }

  // Code for keyboardWillHide method
  keyboardWillHide = (event) => {
    if(Platform.OS === 'android'){
      duration = 100
    } else{
      duration = event.duration
    }
    Animated.timing(this.keyboardHeight, {
      duration: duration,
      toValue: 5
    }).start()
  }

  componentDidMount(){
    // this.refs.billAmount.focus();
  }

  componentWillUnmount(){
    this.keyboardWillShow.remove();
    this.keyboardWillHide.remove();
    this.keyboardDidShow.remove();
    this.keyboardDidHide.remove();
  }

  onSelectOption = (id) => {
    var temp = this.state.paymentOptions;
    for(let i=0; i<temp.length; i++){
      temp[i].optionSelected = false;
    }
    temp[id].optionSelected = !temp[id].optionSelected;
    this.setState({
      paymentOptions: temp,
    })
  }

  render() {
    return (
      <SafeAreaView style={styles.safeAreaView}>
      <StatusBar backgroundColor="rgb(0, 187, 181)" barStyle="light-content" />
        <View style={{flex: 1,}}>
          {/* code for background Layout */}
          <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#00d1c1', '#00d1dc']} style={styles.backgroundUpperPart}></LinearGradient>
          <View style={styles.backgroundLowerPart}></View>

          {/* Code for absolute position Layout */}
          <View style={styles.absoluteStyle}>

            {/* code for layer one */}
             <View style={styles.detailsField}>
                <View style={styles.detailsFieldFlex}>
                  <View>
                    <Text style={styles.usernameStyle}>Roshan Sam</Text>
                    <Text style={styles.mobileNumberStyle}>8919807032</Text>
                  </View>
                  <View>
                    <TouchableOpacity style={styles.buttonStyle1}>
                      <Text style={styles.buttonStyle1Text}>CHANGE</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{marginTop: 8, }}>
                  <TouchableOpacity style={{marginRight: 20, }}>
                    <Image source={require('./src/images/close.png')} style={{width: 12, height: 12}} />
                  </TouchableOpacity>
                </View>
             </View> 

             {/* code for layer two */}
              <View style={styles.textInputField}>
                <View style={styles.textInputFieldBox}>
                  <Text style={styles.textStyle}>₹</Text>
                  <TextInput 
                    ref="billAmount"
                    style={styles.textInputStyle}
                    placeholder="Enter Bill Amount"
                    onChangeText={(text) => this.setState({ amount: text })}
                    value={this.state.amount}
                    placeholderTextColor="rgb(147, 232, 228)"
                    keyboardType="number-pad"
                    maxLength={10}
                    />
                </View>
              </View>

              {/* code for layer three */}
              <View style={styles.optionsField}>
                <View style={styles.optionsHeading}>
                  <Text style={styles.optionsHeadingTextStyle}>Pay using</Text>
                </View>
                <View style={styles.optionViewStyle}>
                  
                  {
                    this.state.paymentOptions.map((item, index) => {
                      return(
                        <SelectOption key={index} optionId={item.optionId} optionHeading={item.optionHeading} optionDescription={item.optionDescription} optionSelected={item.optionSelected} onSelectOption={this.onSelectOption}/>
                      )
                    })
                  }
                </View>
              </View>
            </View>

            {/* code for Pay Bill Button */}
            <Animated.View style={{
                position: 'absolute',
                bottom: this.keyboardHeight,
                width: '100%', 
                justifyContent: 'center', 
                alignItems: 'center', 
            }}>
              <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#00d1c1', '#00d1dc']} style={styles.buttonStyle}>
                <TouchableOpacity style={{flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>
                  <Image source={require('./src/images/lock.png')} style={{width: 10, height: 12, marginRight: 5,}} />
                  <Text style={styles.buttonTextStyle}>PAY BILL</Text>
                </TouchableOpacity>
              </LinearGradient>
            </Animated.View>

        </View>
      </SafeAreaView>
    );
  }
}

